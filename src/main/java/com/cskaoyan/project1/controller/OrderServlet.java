package com.cskaoyan.project1.controller;

import com.cskaoyan.project1.model.Result;
import com.cskaoyan.project1.model.bo.OrderChangeBO;
import com.cskaoyan.project1.model.bo.PageOrderBO;
import com.cskaoyan.project1.model.vo.OrderVO;
import com.cskaoyan.project1.model.vo.PageOrdersVO;
import com.cskaoyan.project1.service.OrderService;
import com.cskaoyan.project1.service.OrderServiceImpl;
import com.cskaoyan.project1.utils.HttpUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/admin/order/*")
public class OrderServlet extends HttpServlet {

    private Gson gson = new Gson();

    private OrderService orderService = new OrderServiceImpl();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String requestURI = request.getRequestURI();
        String action = requestURI.replace("/api/admin/order/", "");
        if ("ordersByPage".equals(action)) {
            ordersByPage(request, response);
        } else if ("changeOrder".equals(action)) {
            changeOrder(request, response);
        }
    }

    private void changeOrder(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestBody = HttpUtils.getRequestBody(request);
        OrderChangeBO orderChangeBO = gson.fromJson(requestBody, OrderChangeBO.class);
        orderService.changeOrder(orderChangeBO);
        response.getWriter().println(gson.toJson(Result.ok()));
    }

    /**
     * 根据条件分页查询显示订单
     * 1.获取请求参数
     * 2.业务处理
     * 3.响应
     * @param request
     * @param response
     */
    private void ordersByPage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestBody = HttpUtils.getRequestBody(request);
        PageOrderBO orderBO = gson.fromJson(requestBody, PageOrderBO.class);
        PageOrdersVO orders = orderService.ordersByPage(orderBO);
        response.getWriter().println(gson.toJson(Result.ok(orders)));
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String requestURI = request.getRequestURI();
        String action = requestURI.replace("/api/admin/order/", "");
        if ("order".equals(action)) {
            order(request, response);
        } else if ("deleteOrder".equals(action)) {
            deleteOrder(request, response);
        }
    }

    private void deleteOrder(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        orderService.deleteOrder(id);
        response.getWriter().println(gson.toJson(Result.ok()));
    }

    private void order(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        OrderVO orderVO = orderService.order(id);
        response.getWriter().println(gson.toJson(Result.ok(orderVO)));
    }
}
