package com.cskaoyan.project1.controller;

import com.cskaoyan.project1.model.Admin;
import com.cskaoyan.project1.model.Result;
import com.cskaoyan.project1.model.bo.AdminAddBO;
import com.cskaoyan.project1.model.bo.AdminChangePwdBO;
import com.cskaoyan.project1.model.bo.AdminLoginBO;
import com.cskaoyan.project1.model.bo.AdminSearchBO;
import com.cskaoyan.project1.model.vo.AdminLoginVO;
import com.cskaoyan.project1.service.AdminService;
import com.cskaoyan.project1.service.AdminServiceImpl;
import com.cskaoyan.project1.utils.HttpUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/api/admin/admin/*")
public class AdminServlet extends HttpServlet {

    private AdminService adminService = new AdminServiceImpl();
    Gson gson = new Gson();


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String requestURI = request.getRequestURI();
        String action = requestURI.replace("/api/admin/admin/", "");
        if ("login".equals(action)) {
            login(request, response);
        } else if ("addAdminss".equals(action)) {
            addAdminss(request, response);
        } else if ("updateAdminss".equals(action)) {
            updateAdminss(request, response);
        } else if ("getSearchAdmins".equals(action)) {
            getSearchAdmins(request, response);
        } else if ("changePwd".equals(action)) {
            changePwd(request, response);
        }
    }

    private void changePwd(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestBody = HttpUtils.getRequestBody(request);
        AdminChangePwdBO changePwdBO = gson.fromJson(requestBody, AdminChangePwdBO.class);
        boolean correct = adminService.checkPwd(changePwdBO);
        if (!correct) {
            response.getWriter().println(gson.toJson(Result.error("旧密码错误!")));
            return;
        }
        if (changePwdBO.getOldPwd().equals(changePwdBO.getNewPwd())) {
            response.getWriter().println(gson.toJson(Result.error("请保持确认新密码一致!")));
            return;
        }

        adminService.changePwd(changePwdBO);

        response.getWriter().println(gson.toJson(Result.ok()));


    }

    private void getSearchAdmins(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestBody = HttpUtils.getRequestBody(request);
        AdminSearchBO searchBO = gson.fromJson(requestBody, AdminSearchBO.class);
        List<Admin> admins = adminService.getSearchAdmins(searchBO);
        response.getWriter().println(gson.toJson(Result.ok(admins)));
    }

    private void updateAdminss(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestBody = HttpUtils.getRequestBody(request);
        Admin admin = gson.fromJson(requestBody, Admin.class);
        int result = adminService.updateAdminss(admin);

        if (result > 0) {
            response.getWriter().println(gson.toJson(Result.ok("修改成功")));
        } else {
            response.getWriter().println(gson.toJson(Result.error("修改失败")));
        }
    }

    private void addAdminss(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestBody = HttpUtils.getRequestBody(request);
        AdminAddBO addBO = gson.fromJson(requestBody, AdminAddBO.class);
        boolean adminss = adminService.addAdminss(addBO);
        if (adminss) {
            response.getWriter().println(gson.toJson(Result.ok("创建管理员成功")));
        } else {
            response.getWriter().println(gson.toJson(Result.error("该账号不允许重复使用")));
        }
    }

    /**
     * 管理员登录的具体业务逻辑
     * 1.浏览器向8084发送一个请求，请求体中携带用户名密码参数（json字符串形式）
     * 2.查询数据库，校验当前用户名密码是否正确
     * 3.根据结果返回不同的响应
     *
     * @param request
     * @param response
     */
    private void login(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //取出请求参数--取出请求体里面的数据
        //此时无法使用request.getParamater
        String requestBody = HttpUtils.getRequestBody(request);
        AdminLoginBO loginBO = gson.fromJson(requestBody, AdminLoginBO.class);
        //System.out.println(loginBO);
        Admin login = adminService.login(loginBO);
        if (login != null) {
            AdminLoginVO loginVO = new AdminLoginVO();
            loginVO.setToken(login.getNickname());
            loginVO.setName(login.getNickname());
            response.getWriter().println(gson.toJson(Result.ok(loginVO)));
        } else {
            response.getWriter().println(gson.toJson(Result.error("用户名或密码错误")));
        }


    }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String requestURI = request.getRequestURI();
        String action = requestURI.replace("/api/admin/admin/", "");
        if ("allAdmins".equals(action)) {
            alladmins(request, response);
        } else if ("deleteAdmins".equals(action)) {
            deleteAdmins(request, response);
        } else if ("getAdminsInfo".equals(action)) {
            getAdminsInfo(request, response);
        }
    }



    private void getAdminsInfo(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        Admin admin = adminService.getAdminsInfo(id);
        response.getWriter().println(gson.toJson(Result.ok(admin)));
    }

    private void deleteAdmins(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = Integer.parseInt(request.getParameter("id"));
        int rusult = adminService.deleteAdmins(id);
        if (rusult > 0) {
            response.getWriter().println(gson.toJson(Result.ok()));
        } else {
            response.getWriter().println(gson.toJson(Result.error("删除失败")));
        }
    }


    /**
     * 显示所有的admin账户信息
     * 1.查询数据库，返回数据
     * 2.做出响应
     *
     * @param request
     * @param response
     */
    private void alladmins(HttpServletRequest request, HttpServletResponse response) throws IOException {
        List<Admin> adminList = adminService.allAdmins();
        Result result = new Result();
        result.setCode(0);
        result.setData(adminList);
        response.getWriter().println(gson.toJson(result));
    }
}
