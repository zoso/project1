package com.cskaoyan.project1.controller;

import com.cskaoyan.project1.model.Result;
import com.cskaoyan.project1.model.Spec;
import com.cskaoyan.project1.model.Type;
import com.cskaoyan.project1.model.bo.GoodsBO;
import com.cskaoyan.project1.model.bo.GoodsUpdateBO;
import com.cskaoyan.project1.model.vo.GoodsInfoVO;
import com.cskaoyan.project1.model.vo.TypeGoodsVO;
import com.cskaoyan.project1.service.GoodsService;
import com.cskaoyan.project1.service.GoodsServiceImpl;
import com.cskaoyan.project1.utils.FileUploadUtils;
import com.cskaoyan.project1.utils.HttpUtils;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebServlet("/api/admin/goods/*")
public class GoodsServlet extends HttpServlet {

    private GoodsService goodsService = new GoodsServiceImpl();

    private Gson gson = new Gson();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String requestURI = request.getRequestURI();
        String action = requestURI.replace("/api/admin/goods/", "");
        if ("imgUpload".equals(action)) {
            imgUpload(request, response);
        } else if ("addGoods".equals(action)) {
            addGoods(request, response);
        } else if ("addType".equals(action)) {
            addType(request, response);
        } else if ("addSpec".equals(action)) {
            addSpec(request, response);
        } else if ("updateGoods".equals(action)) {
            updateGoods(request, response);
        } else if ("deleteSpec".equals(action)) {
            deleteSpec(request, response);
        }
    }

    private void deleteSpec(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestBody = HttpUtils.getRequestBody(request);
        Integer goodsId = gson.fromJson(requestBody, Spec.class).getGoodsId();
        String specName = gson.fromJson(requestBody, Spec.class).getSpecName();
        goodsService.deleteSpec(specName, goodsId);
        response.getWriter().println(gson.toJson(Result.ok()));
    }

    private void updateGoods(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestBody = HttpUtils.getRequestBody(request);
        GoodsUpdateBO goodsUpdateBO = gson.fromJson(requestBody, GoodsUpdateBO.class);
        goodsService.updateGoods(goodsUpdateBO);
        response.getWriter().println(gson.toJson(Result.ok()));
    }

    private void addSpec(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestBody = HttpUtils.getRequestBody(request);
        Spec spec = gson.fromJson(requestBody, Spec.class);
        goodsService.addSpec(spec);
        response.getWriter().println(gson.toJson(Result.ok(spec)));
    }

    private void addType(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestBody = HttpUtils.getRequestBody(request);
        String name = gson.fromJson(requestBody, Type.class).getName();
        goodsService.addType(name);
        response.getWriter().println(gson.toJson(Result.ok()));
    }

    /**
     * 保存商品的业务逻辑
     * 1.获取请求体中的参数
     * 2.处理业务逻辑
     * 3.做出响应
     * @param request
     * @param response
     * @throws IOException
     */
    private void addGoods(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestBody = HttpUtils.getRequestBody(request);
        GoodsBO goodsBO = gson.fromJson(requestBody, GoodsBO.class);
        goodsService.addGoods(goodsBO);
        response.getWriter().println(gson.toJson(Result.ok()));

    }

    /**
     * 新增商品图片的业务逻辑
     * commons-fileupload
     * 1.执行具体的业务逻辑，上传图片
     * 2.响应---抓服务器上的响应报文
     * 3.
     * @param request
     * @param response
     */
    private void imgUpload(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String, Object> map = FileUploadUtils.parseRequest(request);
        String file = (String) map.get("file");
        //这里面有一点需要特别注意：
        //服务器上面的路径没有携带域名端口号，那么就表示图片就从当前域名端口去取
        //如果你的图片和页面不在一个域内，那么应当指明你文件所在的域
        //   http://115.29.141.32:8085/static/image/xxx
        //   file  是从Localhost:8085取的，而我们的图片在后端服务器8084
        //用全路径 http://localhost:8084
        Object domain =  getServletContext().getAttribute("domain");
        response.getWriter().println(gson.toJson(Result.ok((Object) (domain+file))));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String requestURI = request.getRequestURI();
        String action = requestURI.replace("/api/admin/goods/", "");
        if ("getType".equals(action)) {
            getType(request, response);
        } else if ("getGoodsByType".equals(action)) {
            getGoodsByType(request,response);
        } else if ("deleteGoods".equals(action)) {
            deleteGoods(request,response);
        } else if ("deleteType".equals(action)) {
            deleteType(request,response);
        } else if ("getGoodsInfo".equals(action)) {
            getGoodsInfo(request,response);
        }
    }

    private void getGoodsInfo(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        GoodsInfoVO goodsInfoVO = goodsService.getGoodsInfo(id);
        System.out.println(goodsInfoVO);
        response.getWriter().println(gson.toJson(Result.ok(goodsInfoVO)));
    }

    private void deleteType(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String typeId = request.getParameter("typeId");
        goodsService.deleteType(typeId);
        response.getWriter().println(gson.toJson(Result.ok()));
    }

    private void deleteGoods(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        goodsService.deleteGoods(id);
        response.getWriter().println(gson.toJson(Result.ok()));
    }

    /**
     * 获取某个分类下的商品信息
     * 1.获取分类id
     * 2.数据库查询
     * 3.按照一定的数据结构返回
     * @param request
     * @param response
     */
    private void getGoodsByType(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String typeId = request.getParameter("typeId");
        //校验
        List<TypeGoodsVO> goodsVOS= goodsService.getGoodsByType(typeId);
        response.getWriter().println(gson.toJson(Result.ok(goodsVOS)));
    }

    /**
     * 加载商品的所有分类
     *
     * @param request
     * @param response
     */
    private void getType(HttpServletRequest request, HttpServletResponse response) throws IOException {
        List<Type> typeList=  goodsService.getType();
        response.getWriter().println(gson.toJson(Result.ok((typeList))));

    }
}
