package com.cskaoyan.project1.dao;

import com.alibaba.druid.util.StringUtils;
import com.cskaoyan.project1.model.Spec;
import com.cskaoyan.project1.model.bo.OrderChangeBO;
import com.cskaoyan.project1.model.bo.PageOrderBO;
import com.cskaoyan.project1.model.vo.OrderCurSpecVO;
import com.cskaoyan.project1.model.vo.OrderCurStateVo;
import com.cskaoyan.project1.model.vo.OrderVO;
import com.cskaoyan.project1.model.vo.PageOrderInfoVO;
import com.cskaoyan.project1.utils.DruidUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrderDaoImpl implements OrderDao {

    /**
     * 分页查询的sql语句
     * select * from where ...... limit ？ offset ？；
     * @param orderBO
     * @return
     */
    @Override
    public List<PageOrderInfoVO> ordersByPage(PageOrderBO orderBO) {
        Map<String, Object> results= getDynamicSql(orderBO);
        String sql = (String) results.get("sql");
        List<Object> params = (List<Object>) results.get("params");
        String prefix_sql = "select id,userId,goodsDetailId,goods,spec,num AS goodsNum,amount,stateId,nickname,name,address,phone ";
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        params.add(orderBO.getPagesize());
        params.add((orderBO.getCurrentPage()-1) * orderBO.getPagesize() );
        List<PageOrderInfoVO> orderInfoVOS = null;
        try {
            orderInfoVOS = runner.query(prefix_sql + sql + " limit ? offset ? ",
                    new BeanListHandler<PageOrderInfoVO>(PageOrderInfoVO.class), params.toArray());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderInfoVOS;
    }

    /**
     * select count(id) form orders where ......
     * @param orderBO
     * @return
     */
    @Override
    public int getTotalCounts(PageOrderBO orderBO) {
        Map<String, Object> results = getDynamicSql(orderBO);
        String sql = (String) results.get("sql");
        List<Object> params = (List<Object>) results.get("params");
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        String prefix_sql = "select count(id) ";
        Long query = null;
        try {
            query = runner.query(prefix_sql + sql, new ScalarHandler<>(), params.toArray());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return query.intValue();
    }

    @Override
    public OrderVO order(String id) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        OrderVO query = null;
        try {
            query = runner.query("select id,amount,num,goodsDetailId,stateId AS state,goods from orders where id = ?", new BeanHandler<>(OrderVO.class), id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return query;
    }

    @Override
    public List<Spec> spec(String id) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        List<Spec> query = null;
        try {
            Integer goodsId = runner.query("select goodsId from orders where id=?", new ScalarHandler<>(), id);
            query = runner.query("select id,specName ,unitPrice from spec where goodsId = ?", new BeanListHandler<Spec>(Spec.class), goodsId.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return query;
    }

    @Override
    public OrderVO cur(OrderVO orderVO) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        Integer curState = null;
        try {
            curState = runner.query("select stateId from orders where id=?", new ScalarHandler<>(), orderVO.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Integer curSpec = null;
        try {
            curSpec = runner.query("select goodsDetailId from orders where id=?", new ScalarHandler<>(), orderVO.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        orderVO.setCurState(new OrderCurStateVo(curState.intValue()));
        orderVO.setCurSpec(new OrderCurSpecVO(curSpec));

        return orderVO;
    }

    @Override
    public void changeOrder(OrderChangeBO orderChangeBO) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("update orders SET  num = ?, goodsDetailId = ?, stateId = ? WHERE id = ?", orderChangeBO.getNum(),orderChangeBO.getSpec(),orderChangeBO.getState(),orderChangeBO.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteOrder(String id) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("delete from orders where id = ?", id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Map<String, Object> getDynamicSql(PageOrderBO orderBO) {
        String base = " from orders where 1=1 ";
        List<Object> list = new ArrayList<>();
        if (orderBO.getState() != -1) {
            base = base + " and stateId = ?";
            list.add(orderBO.getState());
        }
        if (!StringUtils.isEmpty(orderBO.getMoneyLimit1())) {
            base = base + " and amount <= ?";
            list.add(Double.parseDouble(orderBO.getMoneyLimit1()));
        }
        if (!StringUtils.isEmpty(orderBO.getMoneyLimit2())) {
            base = base + " and amount >= ?";
            list.add(Double.parseDouble(orderBO.getMoneyLimit2()));
        }
        if (!StringUtils.isEmpty(orderBO.getGoods())) {
            base = base + " and goods like ?";
            list.add("%" + orderBO.getGoods() + "%");
        }
        if (!StringUtils.isEmpty(orderBO.getAddress())) {
            base = base + " and address like ?";
            list.add("%" + orderBO.getAddress() + "%");
        }
        if (!StringUtils.isEmpty(orderBO.getName())) {
            base = base + " and name like ?";
            list.add("%" + orderBO.getName() + "%");
        }
        if (!StringUtils.isEmpty(orderBO.getId())) {
            base = base + " and id = ?";
            list.add(Integer.parseInt(orderBO.getId()));
        }

        Map<String, Object> map = new HashMap<>();
        map.put("sql", base);
        map.put("params", list);
        return map;
    }
}
