package com.cskaoyan.project1.dao;

import com.cskaoyan.project1.model.Goods;
import com.cskaoyan.project1.model.Spec;
import com.cskaoyan.project1.model.Type;
import com.cskaoyan.project1.model.vo.GoodsVo;
import com.cskaoyan.project1.model.vo.TypeGoodsVO;

import java.util.List;

public interface GoodsDao {
    List<Type> getType();

    List<TypeGoodsVO> getGoodsByType(String typeId);

    void addGoods(Goods goods);

    int lastInsertId();

    void addSpecs(List<Spec> specs);

    void deleteGoods(String id);

    void deleteGoodsSpecs(String id);

    void addType(String name);

    void deleteType(String typeId);

    void deleteTypeGoods(String typeId);


    List<Spec> getSpecs(String id);

    GoodsVo getGoodsVO(String id);

    void addSpec(Spec spec);

    void updateGoods(Goods goods);

    void updateSpec(Spec specUpdate);

    void deleteSpec(String specName, Integer goodsId);
}
