package com.cskaoyan.project1.dao;

import com.alibaba.druid.util.StringUtils;
import com.cskaoyan.project1.model.Admin;
import com.cskaoyan.project1.model.User;
import com.cskaoyan.project1.model.bo.AdminAddBO;
import com.cskaoyan.project1.model.bo.AdminChangePwdBO;
import com.cskaoyan.project1.utils.DruidUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdminDaoImpl implements AdminDao {
    private static int id = 300;

    @Override
    public Admin login(Admin admin) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        Admin query = null;
        try {
            query = runner.query("select * from admin where email = ? and pwd = ?", new BeanHandler<>(Admin.class), admin.getEmail(), admin.getPwd());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return query;
    }

    @Override
    public List<Admin> allAdmins() {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        List<Admin> admins = null;
        try {
            admins = runner.query("select * from admin ", new BeanListHandler<Admin>(Admin.class));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return admins;
    }

    @Override
    public int deleteAdmins(int id) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        int result = 0;
        try {
            result = runner.update("delete from admin where id = ?", id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void addAdminss(AdminAddBO addBO) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        String sql = "insert admin value(?,?,?,?)";
        Object[] para = {++id, addBO.getEmail(), addBO.getPwd(), addBO.getNickname()};
        try {
            runner.update(sql, para);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Admin getAdminsInfo(int id) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        Admin admin = null;
        try {
            admin = runner.query("select * from admin where id=?", new BeanHandler<>(Admin.class), id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return admin;
    }

    @Override
    public int updateAdminss(Admin admin) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        int result = 0;
        try {
            result = runner.update("update admin SET email=? , nickname=? ,pwd=? where id=?", admin.getEmail(), admin.getNickname(), admin.getPwd(), admin.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<Admin> getSearchAdmins(Admin admin) {
        Map<String, Object> result = getDynamicSql(admin);
        String sql = (String) result.get("sql");
        List<String> params = (List<String>) result.get("params");
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        List<Admin> admins = null;
        try {
            admins = runner.query(sql, new BeanListHandler<Admin>(Admin.class), params.toArray());
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return admins;
    }

    @Override
    public Admin checkPwd(Admin admin) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        Admin query = null;
        try {
            query = runner.query("select * from admin where nickname=? and pwd=?", new BeanHandler<>(Admin.class), admin.getNickname(),admin.getPwd());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return query;
    }

    @Override
    public void changePwd(AdminChangePwdBO changePwdBO) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("update admin SET pwd=? where nickname=?",changePwdBO.getNewPwd(),changePwdBO.getAdminToken() );
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> allUser(HttpServletRequest request, HttpServletResponse response) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        List<User> query = null;
        try {
            query = runner.query("select * from user", new BeanListHandler<User>(User.class));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return query;
    }

    private Map<String, Object> getDynamicSql(Admin admin) {
        String base = "select * from admin where 1=1 ";
        List<String> params = new ArrayList<>();
        if (!StringUtils.isEmpty(admin.getEmail())) {
            base = base + "and email like ?";
            params.add("%" + admin.getEmail() + "%");
        }
        if (!StringUtils.isEmpty(admin.getNickname())) {
            base = base + "and nickname like ?";
            params.add("%" + admin.getNickname() + "%");
        }
        Map<String, Object> map = new HashMap<>();
        map.put("sql", base);
        map.put("params", params);
        return map;
    }
}
