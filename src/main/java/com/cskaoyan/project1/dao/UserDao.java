package com.cskaoyan.project1.dao;

import com.cskaoyan.project1.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface UserDao {
    List<User> allUser(HttpServletRequest request, HttpServletResponse response);

    void deleteUser(String id);

    List<User> searchUser(String word);
}
