package com.cskaoyan.project1.dao;

import com.cskaoyan.project1.model.Spec;
import com.cskaoyan.project1.model.bo.OrderChangeBO;
import com.cskaoyan.project1.model.bo.PageOrderBO;
import com.cskaoyan.project1.model.vo.OrderVO;
import com.cskaoyan.project1.model.vo.PageOrderInfoVO;

import java.util.List;

public interface OrderDao {
    List<PageOrderInfoVO> ordersByPage(PageOrderBO orderBO);

    int getTotalCounts(PageOrderBO orderBO);

    OrderVO order(String id);

    List<Spec> spec(String id);

    OrderVO cur(OrderVO orderVO);

    void changeOrder(OrderChangeBO orderChangeBO);

    void deleteOrder(String id);
}
