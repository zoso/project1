package com.cskaoyan.project1.dao;

import com.cskaoyan.project1.model.Goods;
import com.cskaoyan.project1.model.Spec;
import com.cskaoyan.project1.model.Type;
import com.cskaoyan.project1.model.vo.GoodsVo;
import com.cskaoyan.project1.model.vo.TypeGoodsVO;
import com.cskaoyan.project1.utils.DruidUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.math.BigInteger;
import java.sql.SQLException;
import java.util.List;

public class GoodsDaoImpl implements GoodsDao {

    private static int typeId = 100;
    @Override
    public List<Type> getType() {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        List<Type> typeList = null;
        try {
            typeList = runner.query("select * from type", new BeanListHandler<Type>(Type.class));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return typeList;
    }

    @Override
    public List<TypeGoodsVO> getGoodsByType(String typeId) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        List<TypeGoodsVO> typeGoodsVOList = null;
        try {
            typeGoodsVOList = runner.query("select id,img,name,price,typeId,stockNum from goods where typeId = ?", new BeanListHandler<TypeGoodsVO>(TypeGoodsVO.class), typeId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return typeGoodsVOList;
    }

    @Override
    public void addGoods(Goods goods) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("insert into goods values (null,?,?,?,?,?,?)", goods.getImg(), goods.getName(), goods.getPrice(), goods.getTypeId(), goods.getStockNum(), goods.getDesc());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int lastInsertId() {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        BigInteger query = null;
        try {
            query = runner.query("select last_insert_id()", new ScalarHandler<>());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return query.intValue();

    }

    @Override
    public void addSpecs(List<Spec> specs) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        for (Spec spec : specs) {
            try {
                runner.update("insert into spec values (null,?,?,?,?)", spec.getSpecName(), spec.getStockNum(), spec.getUnitPrice(), spec.getGoodsId());
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void deleteGoods(String id) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("delete from goods where id = ?", id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteGoodsSpecs(String id) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("delete from spec where goodsId = ?", id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addType(String name) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("insert into type value(?,?)", ++typeId,name);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteType(String typeId) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("delete from type where id = ?", typeId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteTypeGoods(String typeId) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("delete from goods where typeId = ?", typeId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public List<Spec> getSpecs(String id) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        List<Spec> specs = null;
        try {
            specs = runner.query("select * from spec where goodsId = ?", new BeanListHandler<Spec>(Spec.class), id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return specs;
    }

    @Override
    public GoodsVo getGoodsVO(String id) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        GoodsVo query = null;
        try {
            query = runner.query("select id,img,name,`desc`,typeId,price AS unitPrice from goods where id = ?", new BeanHandler<>(GoodsVo.class), id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return query;
    }

    @Override
    public void addSpec(Spec spec) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("insert into spec value(null,?,?,?,?)", spec.getSpecName(),spec.getStockNum(),spec.getUnitPrice(),spec.getGoodsId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateGoods(Goods goods) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("update goods SET  name = ?, typeId = ?, img = ?, `desc` = ?, price = ?, stockNum = ? WHERE id = ?", goods.getName(), goods.getTypeId(), goods.getImg(), goods.getDesc(), goods.getPrice(), goods.getStockNum(),goods.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateSpec(Spec specUpdate) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("update spec SET  specName = ?, stockNum = ?, unitPrice = ? WHERE id = ?", specUpdate.getSpecName(),specUpdate.getStockNum(),specUpdate.getUnitPrice(),specUpdate.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteSpec(String specName, Integer goodsId) {
        QueryRunner runner = new QueryRunner(DruidUtils.getDataSource());
        try {
            runner.update("delete from spec where specName = ? and goodsId = ?",specName,goodsId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
