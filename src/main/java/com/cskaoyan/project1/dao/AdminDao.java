package com.cskaoyan.project1.dao;

import com.cskaoyan.project1.model.Admin;
import com.cskaoyan.project1.model.User;
import com.cskaoyan.project1.model.bo.AdminAddBO;
import com.cskaoyan.project1.model.bo.AdminChangePwdBO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface AdminDao {
    Admin login(Admin admin);

    List<Admin> allAdmins();

    int deleteAdmins(int id);

    void addAdminss(AdminAddBO addBO);

    Admin getAdminsInfo(int id);

    int updateAdminss(Admin admin);

    List<Admin> getSearchAdmins(Admin admin);

    Admin checkPwd(Admin admin);

    void changePwd(AdminChangePwdBO changePwdBO);

    List<User> allUser(HttpServletRequest request, HttpServletResponse response);
}
