package com.cskaoyan.project1.model.vo;

public class OrderCurSpecVO {
    private Integer id;

    public OrderCurSpecVO() {
    }

    public OrderCurSpecVO(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
