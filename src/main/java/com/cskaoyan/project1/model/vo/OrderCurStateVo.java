package com.cskaoyan.project1.model.vo;

public class OrderCurStateVo {
    private int id;


    public OrderCurStateVo() {
    }

    public OrderCurStateVo(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
