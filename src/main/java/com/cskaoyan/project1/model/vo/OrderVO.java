package com.cskaoyan.project1.model.vo;

import com.cskaoyan.project1.model.Spec;

import java.util.ArrayList;
import java.util.List;

public class OrderVO {

    private Integer id;

    private Double amount;

    private Integer num;

    private Integer goodsDetailId;

    private Integer state;

    private String goods;

    private List<Spec> spec;

    private List<OrderStateVO> states = new ArrayList<>();

    private OrderCurStateVo curState=new OrderCurStateVo();

    private OrderCurSpecVO curSpec= new OrderCurSpecVO();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getGoodsDetailId() {
        return goodsDetailId;
    }

    public void setGoodsDetailId(Integer goodsDetailId) {
        this.goodsDetailId = goodsDetailId;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getGoods() {
        return goods;
    }

    public void setGoods(String goods) {
        this.goods = goods;
    }

    public List<Spec> getSpec() {
        return spec;
    }

    public void setSpec(List<Spec> spec) {
        this.spec = spec;
    }

    public List<OrderStateVO> getStates() {
        return states;
    }

    public void setStates(List<OrderStateVO> states) {
        this.states = states;
    }

    public OrderCurStateVo getCurState() {
        return curState;
    }

    public void setCurState(OrderCurStateVo curState) {
        states.add(new OrderStateVO(0,"未付款"));
        states.add(new OrderStateVO(1,"未发货"));
        states.add(new OrderStateVO(2,"已发货"));
        states.add(new OrderStateVO(3,"已完成订单"));
        this.curState = curState;
    }

    public OrderCurSpecVO getCurSpec() {
        return curSpec;
    }

    public void setCurSpec(OrderCurSpecVO curSpec) {
        this.curSpec = curSpec;
    }
}
