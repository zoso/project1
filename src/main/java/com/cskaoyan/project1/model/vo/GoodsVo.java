package com.cskaoyan.project1.model.vo;

public class GoodsVo {
    private Integer id;

    private String img;

    private String name;

    private String desc;

    private Integer typeId;

    private Double unitPrice;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public GoodsVo() {
    }

    public GoodsVo(Integer id, String img, String name, String desc, Integer typeId, Double unitPrice) {
        this.id = id;
        this.img = img;
        this.name = name;
        this.desc = desc;
        this.typeId = typeId;
        this.unitPrice = unitPrice;
    }

    @Override
    public String toString() {
        return "GoodsVo{" +
                "id=" + id +
                ", img='" + img + '\'' +
                ", name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", typeId=" + typeId +
                ", unitPrice=" + unitPrice +
                '}';
    }
}
