package com.cskaoyan.project1.model.vo;

import com.cskaoyan.project1.model.Spec;

import java.util.List;

public class GoodsInfoVO {

    private List<Spec> specs;

    private GoodsVo goods;

    public List<Spec> getSpecs() {
        return specs;
    }

    public void setSpecs(List<Spec> specs) {
        this.specs = specs;
    }

    public GoodsVo getGoods() {
        return goods;
    }

    public void setGoods(GoodsVo goods) {
        this.goods = goods;
    }
}
