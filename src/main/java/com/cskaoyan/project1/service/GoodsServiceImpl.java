package com.cskaoyan.project1.service;

import com.cskaoyan.project1.dao.GoodsDao;
import com.cskaoyan.project1.dao.GoodsDaoImpl;
import com.cskaoyan.project1.model.Goods;
import com.cskaoyan.project1.model.Spec;
import com.cskaoyan.project1.model.Type;
import com.cskaoyan.project1.model.bo.GoodsBO;
import com.cskaoyan.project1.model.bo.GoodsUpdateBO;
import com.cskaoyan.project1.model.bo.SpecBO;
import com.cskaoyan.project1.model.vo.GoodsInfoVO;
import com.cskaoyan.project1.model.vo.GoodsVo;
import com.cskaoyan.project1.model.vo.TypeGoodsVO;

import java.util.ArrayList;
import java.util.List;

public class GoodsServiceImpl implements GoodsService {

    private GoodsDao goodsDao = new GoodsDaoImpl();

    @Override
    public List<Type> getType() {
        return goodsDao.getType();
    }

    @Override
    public List<TypeGoodsVO> getGoodsByType(String typeId) {
        return goodsDao.getGoodsByType(typeId);
    }

    /**
     * 新增商品表price，stockNum需要通过specList运算得到
     * 1.保存数据到商品表
     * 2.拿到商品表刚刚插入的商品id
     * 3.将该id以及spec数据保存到spec规格表
     *
     * @param goodsBO
     */
    @Override
    public void addGoods(GoodsBO goodsBO) {
        List<SpecBO> specList = goodsBO.getSpecList();
        Double price = specList.get(0).getUnitPrice();
        Integer stockNum = specList.get(0).getStockNum();
        for (int i = 1; i < specList.size(); i++) {
            if (price > specList.get(i).getUnitPrice()) {
                price = specList.get(i).getUnitPrice();
                stockNum = specList.get(i).getStockNum();
            }
        }
        Goods goods = new Goods(null, goodsBO.getImg(), goodsBO.getName(), price, goodsBO.getTypeId(), stockNum, goodsBO.getDesc());
        goodsDao.addGoods(goods);
        //gaiid值就是goods表新增的商品的id
        int id = goodsDao.lastInsertId();
        List<Spec> specs = new ArrayList<>();
        for (SpecBO specBO : specList) {
            Spec spec = new Spec(null, specBO.getSpecName(), specBO.getStockNum(), specBO.getUnitPrice(), id);
            specs.add(spec);
        }
        goodsDao.addSpecs(specs);
    }

    @Override
    public void deleteGoods(String id) {
        goodsDao.deleteGoods(id);
        goodsDao.deleteGoodsSpecs(id);
    }

    @Override
    public void addType(String name) {
        goodsDao.addType(name);
    }

    @Override
    public void deleteType(String typeId) {
        goodsDao.deleteType(typeId);
        goodsDao.deleteTypeGoods(typeId);
    }

    @Override
    public GoodsInfoVO getGoodsInfo(String id) {
        GoodsInfoVO goodsInfoVO = new GoodsInfoVO();
        GoodsVo goodsVo = goodsDao.getGoodsVO(id);
        goodsInfoVO.setGoods(goodsVo);
        List<Spec> specs = goodsDao.getSpecs(id);
        goodsInfoVO.setSpecs(specs);
        return goodsInfoVO;
    }

    @Override
    public void addSpec(Spec spec) {
        goodsDao.addSpec(spec);
    }

    @Override
    public void updateGoods(GoodsUpdateBO goodsUpdateBO) {
        List<Spec> specList = goodsUpdateBO.getSpecList();
        Double price = specList.get(0).getUnitPrice();
        Integer stockNum = specList.get(0).getStockNum();
        for (int i = 1; i < specList.size(); i++) {
            if (price > specList.get(i).getUnitPrice()) {
                price = specList.get(i).getUnitPrice();
                stockNum = specList.get(i).getStockNum();
            }
        }
        Goods goods = new Goods(goodsUpdateBO.getId(), goodsUpdateBO.getImg(), goodsUpdateBO.getName(), price, goodsUpdateBO.getTypeId(), stockNum, goodsUpdateBO.getDesc());
        goodsDao.updateGoods(goods);

        //gaiid值就是goods表新增的商品的id
        int goodsId = goodsUpdateBO.getId();
        for (Spec spec : specList) {
            Spec specUpdate = new Spec(spec.getId(), spec.getSpecName(), spec.getStockNum(), spec.getUnitPrice(), goodsId);
            goodsDao.updateSpec(specUpdate);
        }
    }

    @Override
    public void deleteSpec(String specName, Integer goodsId) {
        goodsDao.deleteSpec(specName, goodsId);
    }

}
