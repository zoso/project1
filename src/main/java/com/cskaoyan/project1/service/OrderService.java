package com.cskaoyan.project1.service;

import com.cskaoyan.project1.model.bo.OrderChangeBO;
import com.cskaoyan.project1.model.bo.PageOrderBO;
import com.cskaoyan.project1.model.vo.OrderVO;
import com.cskaoyan.project1.model.vo.PageOrdersVO;

public interface OrderService {
    PageOrdersVO ordersByPage(PageOrderBO pageOrderBO);

    OrderVO order(String id);

    void changeOrder(OrderChangeBO orderChangeBO);

    void deleteOrder(String id);
}
