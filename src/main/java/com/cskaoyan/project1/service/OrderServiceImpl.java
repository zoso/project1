package com.cskaoyan.project1.service;

import com.cskaoyan.project1.dao.OrderDao;
import com.cskaoyan.project1.dao.OrderDaoImpl;
import com.cskaoyan.project1.model.bo.OrderChangeBO;
import com.cskaoyan.project1.model.bo.PageOrderBO;
import com.cskaoyan.project1.model.vo.OrderVO;
import com.cskaoyan.project1.model.vo.PageOrderInfoVO;
import com.cskaoyan.project1.model.vo.PageOrdersVO;

import java.util.List;

public class OrderServiceImpl implements OrderService {

    private OrderDao orderDao = new OrderDaoImpl();

    /**
     * 根据传入的不同参数，执行不同的查询，返回对应的结果
     * @param orderBO
     * @return
     */
    @Override
    public PageOrdersVO ordersByPage(PageOrderBO orderBO) {
        int totalCounts= orderDao.getTotalCounts(orderBO);
        //查询当前分页结果page1  1-5   page2 6-10  page3 11-15
        List<PageOrderInfoVO> orderInfoVOS=  orderDao.ordersByPage(orderBO);
        //for---取出每一个stateID----state


        PageOrdersVO pageOrdersVO = new PageOrdersVO();
        pageOrdersVO.setTotal(totalCounts);
        pageOrdersVO.setOrders(orderInfoVOS);
        return  pageOrdersVO;
    }

    @Override
    public OrderVO order(String id) {
        OrderVO orderVO=orderDao.order(id);
        orderVO.setSpec(orderDao.spec(id));
        OrderVO cur=orderDao.cur(orderVO);
        return cur;
    }

    @Override
    public void changeOrder(OrderChangeBO orderChangeBO) {
        orderDao.changeOrder(orderChangeBO);
    }

    @Override
    public void deleteOrder(String id) {
        orderDao.deleteOrder(id);
    }
}
