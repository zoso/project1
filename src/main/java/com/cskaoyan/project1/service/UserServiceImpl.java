package com.cskaoyan.project1.service;

import com.cskaoyan.project1.dao.UserDao;
import com.cskaoyan.project1.dao.UserDaoImpl;
import com.cskaoyan.project1.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDao userDao = new UserDaoImpl();

    @Override
    public List<User> allUser(HttpServletRequest request, HttpServletResponse response) {
        return userDao.allUser(request,response);
    }

    @Override
    public void deleteUser(String id) {
        userDao.deleteUser(id);
    }

    @Override
    public List<User> searchUser(String word) {
        return userDao.searchUser(word);
    }

}
