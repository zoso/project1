package com.cskaoyan.project1.service;

import com.cskaoyan.project1.dao.AdminDao;
import com.cskaoyan.project1.dao.AdminDaoImpl;
import com.cskaoyan.project1.model.Admin;
import com.cskaoyan.project1.model.bo.AdminAddBO;
import com.cskaoyan.project1.model.bo.AdminChangePwdBO;
import com.cskaoyan.project1.model.bo.AdminLoginBO;
import com.cskaoyan.project1.model.bo.AdminSearchBO;

import java.util.List;

public class AdminServiceImpl implements AdminService {

    private AdminDao adminDao = new AdminDaoImpl();



    @Override
    public Admin login(AdminLoginBO loginBO) {
        Admin admin = new Admin();
        admin.setEmail(loginBO.getEmail());
        admin.setPwd(loginBO.getPwd());
        return adminDao.login(admin);
    }

    @Override
    public List<Admin> allAdmins() {
        return adminDao.allAdmins();
    }

    @Override
    public int deleteAdmins(int id) {
        return adminDao.deleteAdmins(id);
    }

    @Override
    public boolean addAdminss(AdminAddBO addBO) {
        List<Admin> admins = adminDao.allAdmins();
        for (Admin admin : admins) {
            if (admin.getEmail().equals(addBO.getEmail()) ) {
                return false;
            }
        }
        adminDao.addAdminss(addBO);
        return true;
    }

    @Override
    public Admin getAdminsInfo(int id) {
        return adminDao.getAdminsInfo(id);
    }

    @Override
    public int updateAdminss(Admin admin) {
        return adminDao.updateAdminss(admin);
    }

    @Override
    public List<Admin> getSearchAdmins(AdminSearchBO searchBO) {
        Admin admin = new Admin();
        admin.setEmail(searchBO.getEmail());
        admin.setNickname(searchBO.getNickname());
        return adminDao.getSearchAdmins(admin);
    }

    @Override
    public boolean checkPwd(AdminChangePwdBO changePwdBO) {
        Admin admin = new Admin();
        admin.setNickname(changePwdBO.getAdminToken());
        admin.setPwd(changePwdBO.getOldPwd());
        return adminDao.checkPwd(admin)!=null;
    }

    @Override
    public void changePwd(AdminChangePwdBO changePwdBO) {
        adminDao.changePwd(changePwdBO);
    }


}
