package com.cskaoyan.project1.service;

import com.cskaoyan.project1.model.Spec;
import com.cskaoyan.project1.model.Type;
import com.cskaoyan.project1.model.bo.GoodsBO;
import com.cskaoyan.project1.model.bo.GoodsUpdateBO;
import com.cskaoyan.project1.model.vo.GoodsInfoVO;
import com.cskaoyan.project1.model.vo.TypeGoodsVO;

import java.util.List;

public interface GoodsService {
    List<Type> getType();

    List<TypeGoodsVO> getGoodsByType(String typeId);

    void addGoods(GoodsBO goodsBO);

    void deleteGoods(String id);

    void addType(String requestBody);

    void deleteType(String typeId);

    GoodsInfoVO getGoodsInfo(String id);

    void addSpec(Spec spec);

    void updateGoods(GoodsUpdateBO goodsUpdateBO);

    void deleteSpec(String specName, Integer goodsId);
}
