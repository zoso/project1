package com.cskaoyan.project1.service;

import com.cskaoyan.project1.model.Admin;
import com.cskaoyan.project1.model.bo.AdminAddBO;
import com.cskaoyan.project1.model.bo.AdminChangePwdBO;
import com.cskaoyan.project1.model.bo.AdminLoginBO;
import com.cskaoyan.project1.model.bo.AdminSearchBO;

import java.util.List;

public interface AdminService {
    Admin login(AdminLoginBO loginBO);

    List<Admin> allAdmins();

    int deleteAdmins(int id);

    boolean addAdminss(AdminAddBO addBO);

    Admin getAdminsInfo(int id);

    int updateAdminss(Admin admin);

    List<Admin> getSearchAdmins(AdminSearchBO searchBO);

    boolean checkPwd(AdminChangePwdBO changePwdBO);

    void changePwd(AdminChangePwdBO changePwdBO);

}
